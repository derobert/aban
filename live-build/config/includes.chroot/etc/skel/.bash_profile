# On the first login, we start ABAN

do_start_aban() {
	local res
	local REPLY
	echo
	echo 'Starting ABAN to wipe all disks...'
	echo

	beep -f 880 -l 250 -n -f 1109 -l 175 -n -f 988 -l 250 -n -f 1175 -l 500 -D 150 -n -f 1319 -l 250 -D 75 -r 3 # A5 C#6 B5 D6 E6 E6 E6
	
	aban-kbd-detect
	res=$?

	if [ $res -eq 1 ]; then
		echo 'Since no keyboard detected, proceeding without confirmation.';
	else
		while true; do
			read -p 'Do you want to wipe all disks (Y/N)? ' -t 3 -r -n 1
			case "$REPLY" in
				Y|y)
					printf '\n';
					break;
					;;
				N|n)
					printf '\n';
					return;
					;;
				*)
					printf '\n';
					beep -f 1319 -l 250 -D 75 -r 3
					;;
			esac
		done
	fi

	${ABAN_SUDO_CMD:-sudo} ABAN DESTROY ALL DATA
	res=$?

	perl -MFcntl -E '$f = fcntl(STDIN, F_GETFL, 0); fcntl(STDIN, F_SETFL, $f & ~O_NONBLOCK)'

	if [ $res -ne 0 ]; then
		echo
		echo "*** ABAN has indicated a failure (code=$res) ***";
	fi
	echo
	echo "ABAN has exited; if you need to start it again, you can run:"
	echo "   sudo ABAN DESTROY ALL DATA"
	echo ""
	echo "(press any key to stop beeping and return to the prompt)"
	until read -t 1 -r -n 1 -s; do
		if [ $res -eq 0 ]; then
			beep -f 1397 -l 175 -n -f 1480 -l 250 # F6 F#6
		else
			# F4 Eb4 F3
			beep -f 349 -l 175 -D 125 -n -f 311 -l 175 -D 125 -n -f 175 -l 500
		fi
	done
}

if ! [ -e ~/.aban-autostarted ]; then
	touch ~/.aban-autostarted
	do_start_aban
fi
