use Test::More;
use strict;
use warnings qw(all);
no warnings qw(portable); # Hexadecimal number > 0xffffffff non-portable
# TODO: the C code asserts 64-bit anyway, someday port to 32-bit.

use App::ABAN::PRF::Nettle;

my $zero = "\0" x 64;
my $out  = " " x 64;

my @vectors = ({
		name  => 'RFC 8439 2.3.2 Test Vector',
		key   => "\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1a\x1b\x1c\x1d\x1e\x1f",
		nonce => "\x00\x00\x00\x4a\x00\x00\x00\x00",
		block => 0x09000000_00000001,
		want  => <<HEX =~ s/\s+//gr,
10 f1 e7 e4 d1 3b 59 15 50 0f dd 1f a3 20 71 c4
c7 d1 f4 c7 33 c0 68 03 04 22 aa 9a c3 d4 6c 4e
d2 82 64 46 07 9f aa 09 14 c2 d7 05 d9 8b 02 a2
b5 12 9c d1 de 16 4e b9 cb d0 83 e8 a2 50 3c 4e
HEX
	},
	{
		name  => 'RFC 8439 A.1 Test Vector 1',
		key   => scalar("\0" x 32),
		nonce => scalar("\0" x 8),
		block => 0,
		want  => <<HEX =~ s/\s+//gr,
76 b8 e0 ad a0 f1 3d 90 40 5d 6a e5 53 86 bd 28
bd d2 19 b8 a0 8d ed 1a a8 36 ef cc 8b 77 0d c7
da 41 59 7c 51 57 48 8d 77 24 e0 3f b8 d8 4a 37
6a 43 b8 f4 15 18 a1 1c c3 87 b6 69 b2 ee 65 86
HEX
	},
	{
		name  => 'RFC 8439 A.1 Test Vector 2',
		key   => scalar("\0" x 32),
		nonce => scalar("\0" x 8),
		block => 1,
		want  => <<HEX =~ s/\s+//gr,
9f 07 e7 be 55 51 38 7a 98 ba 97 7c 73 2d 08 0d
cb 0f 29 a0 48 e3 65 69 12 c6 53 3e 32 ee 7a ed
29 b7 21 76 9c e6 4e 43 d5 71 33 b0 74 d8 39 d5
31 ed 1f 28 51 0a fb 45 ac e1 0a 1f 4b 79 4d 6f
HEX
	},
	{
		name  => 'RFC 8439 A.1 Test Vector 3',
		key   => scalar("\0" x 31 . "\1"),
		nonce => scalar("\0" x 8),
		block => 1,
		want  => <<HEX =~ s/\s+//gr,
3a eb 52 24 ec f8 49 92 9b 9d 82 8d b1 ce d4 dd
83 20 25 e8 01 8b 81 60 b8 22 84 f3 c9 49 aa 5a
8e ca 00 bb b4 a7 3b da d1 92 b5 c4 2f 73 f2 fd
4e 27 36 44 c8 b3 61 25 a6 4a dd eb 00 6c 13 a0
HEX
	},
	{
		name  => 'RFC 8439 A.1 Test Vector 4',
		key   => scalar("\0\xff" . "\0"x30),
		nonce => scalar("\0" x 8),
		block => 2,
		want  => <<HEX =~ s/\s+//gr,
72 d5 4d fb f1 2e c4 4b 36 26 92 df 94 13 7f 32
8f ea 8d a7 39 90 26 5e c1 bb be a1 ae 9a f0 ca
13 b2 5a a2 6c b4 a6 48 cb 9b 9d 1b e6 5b 2c 09
24 a6 6c 54 d5 45 ec 1b 73 74 f4 87 2e 99 f0 96
HEX
	},
	{
		name  => 'RFC 8439 A.1 Test Vector 5',
		key   => scalar("\0" x 32),
		nonce => scalar("\0" x 7 . "\2"),
		block => 0,
		want  => <<HEX =~ s/\s+//gr,
c2 c6 4d 37 8c d5 36 37 4a e2 04 b9 ef 93 3f cd
1a 8b 22 88 b3 df a4 96 72 ab 76 5b 54 ee 27 c7
8a 97 0e 0e 95 5c 14 f3 a8 8e 74 1b 97 c2 86 f7
5f 8f c2 99 e8 14 83 62 fa 19 8a 39 53 1b ed 6d
HEX
	},
);

plan tests => scalar(@vectors);

foreach my $v (@vectors) {
	App::ABAN::PRF::Nettle::chacha_prf($v->{block}, $v->{key},
		$v->{nonce}, $zero, 0, 64, $out);
	is(unpack('H*', $out), $v->{want}, $v->{name});
}
