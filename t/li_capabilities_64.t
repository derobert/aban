use Test::More;
use 5.028;
use strict;
use warnings qw(all);

use Config;
use Linux::Input::Capabilities;
use Linux::Input::Capabilities::Constants;

# 32-bit and 64-bit use different formats, unfortunately. It probably
# actually depends on the running kernel, but since we're not actualy
# using /sys for the tests, we can feed it the appropriate one, even
# when in a 32-bit docker container on a 64-bit host.
if (64 != 8 * $Config{longsize}) {
	plan skip_all => 'Test only for 64-bit machines';
	return;
} else {
	plan tests => 11;
}

my $devs = new_ok('Linux::Input::Capabilities',
	[sysfs_root => "t-data/input-capabilities/sys-64"]);

is(scalar $devs->list, 6, 'Found expected number of devices');
is(
	$devs->get(0)->name,
	'AT Translated Set 2 keyboard',
	'Device 0 has expected name'
);
is(scalar @{$devs->get(0)->get_supported('ev')},
	5, 'Expected number of libinput things');
is_deeply($devs->get(0)->get_supported('led'),
	[qw(LED_NUML LED_CAPSL LED_SCROLLL)], 'Expected keyboard LEDs');

my @kbd;
{   
	# you can do this with Symbol, like:
	#    *{qualify_to_ref($_, 'Linux::Input::Capabilities::Constants')}->()
	# but that's vomit-worthy so just disable strictures.
	no strict 'refs';
	@kbd = map("Linux::Input::Capabilities::Constants::$_"->(),
		map("KEY_$_", 0 .. 9, 'A' .. 'Z'));
}
is($devs->get(0)->count_supported(key => @kbd), scalar @kbd, 'Keyboard supports all alphanumeric keys');

is(
	$devs->get(2)->name,
	'Logitech USB-PS/2 Optical Mouse',
	'Device 2 has expected name'
);
ok(
	$devs->get(2)
		->is_supported(rel => Linux::Input::Capabilities::Constants::REL_X),
	'Mouse supports moving horizontally'
);
ok(
	$devs->get(2)
		->is_supported(rel => Linux::Input::Capabilities::Constants::REL_Y),
	'Mouse supports moving vertically'
);
ok(
	$devs->get(2)
		->is_supported(key => Linux::Input::Capabilities::Constants::BTN_MOUSE),
	'Mouse supports clicking'
);

is($devs->find('key', scalar(@kbd), @kbd), 2, 'Found correct number of keyboard-like devices');

__END__
use Data::Dump qw(pp);
while (my ($i, $d) = each %{$devs->_devices}) {
	next unless 2 == $i ;
	pp {
		input => $i,
		name => $d->name,
		abs  => $d->get_supported('abs'),
		ev   => $d->get_supported('ev'),
		ff   => $d->get_supported('ff'),
		key  => $d->get_supported('key'),
		led  => $d->get_supported('led'),
		msc  => $d->get_supported('msc'),
		rel  => $d->get_supported('rel'),
		snd  => $d->get_supported('snd'),
		sw   => $d->get_supported('sw'),
	};
}

my $dev0 = $devs->get(0);
