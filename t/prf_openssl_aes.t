use Test::More;
use strict;
use warnings qw(all);
no warnings qw(portable); # Hexadecimal number > 0xffffffff non-portable
# TODO: the C code asserts 64-bit anyway, someday port to 32-bit.

use App::ABAN::PRF::OpenSSL;

my $zero = "\0" x 64;
my $out  = " " x 64;

# Note that for compat with ChaCha20, we split the initial counter into
# a block (counter) and an nonce. If you have a disk over 2^68 bytes, I
# guess that might matter.
my @vectors = ({
		name  => 'NIST 800-38A 2001 F.5.5 #1-4',
		key   => pack("H*", "603deb1015ca71be2b73aef0857d77811f352c073b6108d72d9810a30914dff4"),
		nonce =>             pack('H*', 'f8f9fafbfcfdfeff'),
		block => unpack('Q', pack('H*', 'f0f1f2f3f4f5f6f7')),
		want  => <<HEX =~ s/\s+//gr,
0bdf7df1591716335e9a8b15c860c502
5a6e699d536119065433863c8f657b94
1bc12c9c01610d5d0d8bd6a3378eca62
2956e1c8693536b1bee99c73a31576b6
HEX
	},
);

plan tests => scalar(@vectors);

foreach my $v (@vectors) {
	App::ABAN::PRF::OpenSSL::aes256_prf($v->{block}, $v->{key},
		$v->{nonce}, $zero, 0, 64, $out);
	is(unpack('H*', $out), $v->{want}, $v->{name});
}
