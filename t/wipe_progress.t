use Test::More tests => 24;
use Test::Exception;
use strict;
use warnings qw(all);

use Time::HiRes qw(sleep);
use App::ABAN::WipeProgress;

my ($last_overall_rate, $last_current_rate);
my $output_count = 0;
my $extra_count;
my $slept = 0;

sub output_func {
	my ($prog, $now) = @_;

	++$output_count;
	$last_overall_rate = $prog->overall_rate;
	$last_current_rate = $prog->current_rate;

	return;
}

sub extra_cb {
	++$extra_count;

	return "Extra!";
}

sub is_eps {
	my($got, $want, $epsilon, $msg) = @_;

	if (abs($got-$want) < $epsilon) {
		pass($msg);
	} else {
		fail("$msg: $got not $want +/- $epsilon");
	}
}

my $prog = new_ok('App::ABAN::WipeProgress' => [{
	_display => \&output_func,
	remaining => 1000,
	first_interval => 1,
	interval => 5,
}]);

lives_ok { $prog->remaining(999) } 'updating remaining lived';
is($output_count, 0, 'No output from immediate progress');
note("Sleeping 1.5 seconds...");
$slept += sleep(1.5);

lives_ok { $prog->remaining(900) } 'update with progress lived';
is($output_count, 1, 'Initial update did output');
is_eps($last_overall_rate, 100/$slept, 5, "overall rate");
is_eps($last_current_rate, 100/$slept, 5, "current rate");
my $lp_slept = $slept;

lives_ok { $prog->remaining(899) } 'update without progress 1';
is($output_count, 1, 'Did not output after initial and before time');

note("Sleeping 1.5 seconds...");
$slept += sleep(1.5);
lives_ok { $prog->remaining(898) } 'update without progress 2';
is($output_count, 1, 'Switched to non-initial time');
is_eps($prog->current_rate, 100/$lp_slept, 5,
	"current rate does not update without output");
is_eps($prog->overall_rate, (1000 - 898) / $slept,
	5, 'overall rate updates even without output');
is_eps($prog->etr, 898*$slept/(1000-898), 5, 'ETR is close enough');

note("Sleeping 3.5 seconds...");
$slept += sleep(3.5);
lives_ok { $prog->remaining(1) } 'update with progress 2';
is($output_count, 2, 'Second output happened');
is_eps($last_overall_rate, 999/$slept, 5, 'overall rate after progress 2');
is_eps($last_current_rate, 899/($slept-$lp_slept), 5, 'current rate after progress 2');

lives_ok { $prog->remaining(0) } 'final update';
lives_ok { $prog->_display(undef) } 'un-set test output';
lives_ok { $prog->extra(\&extra_cb) } 'set extra callback';
lives_ok { $prog->done } 'done() method';
is($output_count, 2, 'display function not called');
is($extra_count, 1, 'extra callback called');
