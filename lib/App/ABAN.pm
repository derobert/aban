package App::ABAN;
#ABSTRACT: Main app module for ABAN disk wiper
use 5.028;
use warnings qw(all);
use strict;

use JSON::MaybeXS qw(decode_json);
use IPC::Run3 qw(run3);
use Data::Dump qw(pp);
use Cwd qw(abs_path);
use File::Slurper qw(read_text);
use App::ABAN::DumbUI;
use App::ABAN::WipeChild;
use App::ABAN::BeepChild;
use List::Util qw(reduce);
use Sub::Name qw(subname);
use POE;

use constant {
	_EX_BIT_DIED      => 1 << 0,    # caught an exception
	_EX_BIT_ABORTED   => 1 << 1,    # user abort
	_EX_BIT_BMFAIL    => 1 << 2,    # benchmark (speed test) failed
	_EX_BIT_WIPECRASH => 1 << 3,    # a wipe/verify process crashed
	_EX_BIT_UNUSED_4  => 1 << 4,
	_EX_BIT_UNUSED_5  => 1 << 5,
	_EX_BIT_UNUSED_6  => 1 << 6,
	_EX_BIT_UNUSED_7  => 1 << 7,
};
our $_EXIT_CODE = 0;

sub run {
	eval {
		POE::Session->create(
			inline_states => {
				_start        => \&_start,
				_stop         => sub { },
				_child        => sub { },
				ui_ready      => \&_ui_ready,
				abort         => \&_abort,
				start_wiping  => \&_do_wipes,
				speed_results => \&_speed_results,
				speed_failed  => \&_speed_failed,
				wipe_start    => _wipe_child_generic(
					state => 'wiping',
					slot  => 'wipe_start_info'
				),
				wipe_complete => _wipe_child_generic(
					state => 'pending',
					slot  => 'wipe_complete_info'
				),
				verify_start => _wipe_child_generic(
					state => 'verifying',
					slot  => 'verify_start_info'
				),
				verify_complete => _wipe_child_generic(
					state => 'pending',
					slot  => 'verify_complete_info',
				),
				wipe_child_done => _wipe_child_generic(
					state => 'done',
					slot  => 'ok_exit_info',
					done  => 1
				),
				wipe_child_fail => _wipe_child_generic(
					state => 'errored',
					slot  => 'errored_info',
					done  => 1,
					error => _EX_BIT_WIPECRASH,
				),
			});
		App::ABAN::DumbUI->create ;
		POE::Kernel->run;
	};
	if ($@) {
		$_EXIT_CODE |= _EX_BIT_DIED;
		print STDERR <<ERR
ABAN has encountered an exception:

$@

Please report this bug to aban-bugs\@derobert.net or via the web at
https://gitlab.com/derobert/aban/issues

Thank you.
ERR
	}

	return $_EXIT_CODE;
}

sub _start {
	$_[KERNEL]->alias_set('MAIN'),
	$_[KERNEL]->sig(INT => 'abort');
	#$_[KERNEL]->sig(QUIT => 'abort');
	
	return;
}

sub _ui_ready {
	$_[KERNEL]->call(UI => set_status => 'Finding protected drives');
	my $protect = find_protected();
	$_[KERNEL]->call(UI => set_protected => $protect);

	$_[KERNEL]->call(UI => set_status => 'Finding wipeable drives');
	my $drives = find_drives();
	$_[KERNEL]->call(UI => set_drives => $drives);
	
	my $victims = { map(($_, $drives->{$_}), grep !exists $protect->{$_}, keys %$drives) };
	$_[KERNEL]->call(UI => set_victims => $victims);
	$_[HEAP]{victims} = $victims;

	App::ABAN::BeepChild->create(Initial => scalar keys %{$_[HEAP]{victims}});

	$_[KERNEL]->call(UI => set_status => 'Running PRF benchmark');
	App::ABAN::WipeChild->create('speed', 'SPEED');
	return;
}

sub _speed_results {
	my $benchmark = $_[ARG0];

	$_[KERNEL]->call(UI => set_benchmark => $benchmark);

	my $prf = reduce { $a->{r} >= $b->{r} ? $a : $b } values %$benchmark;
	$prf = $prf->{alg};
	$_[KERNEL]->call(UI => set_prf => $prf);
	$_[HEAP]{prf} = $prf;


	$_[KERNEL]->call(UI => set_status => 'Last Control-C chance!');
	$_[KERNEL]->delay(start_wiping => 30);

	return;
}

sub _speed_failed {
	my $status = $_[ARG0];
	$_[KERNEL]->call(UI => set_status => 'Failed');
	$_[KERNEL]->call('UI', 'shutdown');

	$_EXIT_CODE |= _EX_BIT_BMFAIL;

	print STDERR <<FAIL;

The wipe-disk command's --speed mode has failed; this probably means
something is *very* broken. Exit status: $status. (See above for
possible error messages.)

This probably means ABAN is very broken, giving up. No disks were wiped.

FAIL

	return;
}

sub _do_wipes {
	$_[KERNEL]->call(UI => set_status => 'Wiping');
	App::ABAN::BeepChild->create(WipeStart => scalar keys %{$_[HEAP]{victims}});

	my @wipers;
	my @wipeinfo;
	foreach my $victim (values %{$_[HEAP]{victims}}) {
		push @wipers, App::ABAN::WipeChild->create('wipe', scalar(@wipers), $_[HEAP]{prf}, $victim->{node});
		push @wipeinfo, {
			victim => $victim,
			state => 'pending',
			wipe_start_info => undef,
			wipe_complete_info => undef,
			verify_start_info => undef,
			verify_complete_info => undef,
		};
	}
	$_[HEAP]{wipers} = \@wipers;
	$_[HEAP]{wipeinfo} = \@wipeinfo;

	$_[HEAP]{stat}{pending} = scalar(@wipers);
	$_[HEAP]{stat}{wiping} = 0;
	$_[HEAP]{stat}{verifying} = 0;
	$_[HEAP]{stat}{done} = 0;
	$_[HEAP]{stat}{errored} = 0;

	return;
}

sub _wipe_child_generic {
	my %args = @_;

	return subname "_wipe_child_generic__$args{slot}" => sub {
		my $child = $_[ARG0];
		my $old_state = $_[HEAP]{wipeinfo}[$child]{state};
		exists $_[HEAP]{stat}{$old_state}
			or die "BUG: invalid old state $old_state";

		my $new_state = $args{state};
		exists $_[HEAP]{stat}{$new_state}
			or die "BUG: invalid new state $new_state";

		--$_[HEAP]{stat}{$old_state};
		++$_[HEAP]{stat}{$new_state};
		$_[HEAP]{wipeinfo}[$child]{state} = $new_state;

		my $p = $_[HEAP]{stat}{pending};
		my $w = $_[HEAP]{stat}{wiping};
		my $v = $_[HEAP]{stat}{verifying};
		my $d = $_[HEAP]{stat}{done};
		my $e = $_[HEAP]{stat}{errored};

		$_[HEAP]{wipeinfo}[$child]{$args{slot}} = $_[ARG1];
		$_[KERNEL]->call(UI => set_wipeinfo => $_[HEAP]{wipeinfo});

		$args{error}
			and $_EXIT_CODE |= $args{error};

		$_[KERNEL]
			->call(UI => set_status => "Running ${p}p/${w}w/${v}v/${d}d/${e}e");

		if ($args{done}) {
			$_[HEAP]{wipers}[$child] = undef; # allow GC
			if ($p + $w + $v == 0) {
				$_[KERNEL]->call(UI => 'wipe_complete');
				$_[KERNEL]->post('UI', 'shutdown');
			}
		}

		return;
	};
}

sub _abort {
	$_[KERNEL]->call(UI => set_status => 'Aborting');
	$_[KERNEL]->alarm(start_wiping => undef); # clear it
	$_[KERNEL]->call($_, 'abort') foreach @{$_[HEAP]{wipers}};
	$_[KERNEL]->post('UI', 'shutdown');
	$_[KERNEL]->sig_handled();

	$_EXIT_CODE |= _EX_BIT_ABORTED;

	return;
}

sub find_protected {
	local $_;

	my $out;
	run3([qw(findmnt -ckJ --real)], \undef, \$out, undef)
		or die "findmnt failed";

	# live should have at most boot disk mounted, and only that. But
	# test VMs may have more.
	my $mounts = decode_json($out);
	my @devs = grep(m{^/} && !m{\[/},
		map($_->{source}, @{$mounts->{filesystems}[0]{children}}),
		$mounts->{filesystems}[0]{source});

	# VM might use LVM
	foreach my $holder (glob('/sys/class/block/*/holders/*')) {
		$holder =~ m!^/sys/class/block/([^/]+)/!
			or die "failed to parse holder $holder";
		push @devs, "/dev/$1";
	}

	# Second way, look for our disk label. This gets encoded without
	# spaces, using \x20 in place of space. That's a bit annoying, we
	# have to double-escape the backslash (once for Perl, once for glob).
	push @devs, map abs_path($_), glob('/dev/disk/by-label/ABAN\\\\x20Disk\\\\x20Wiper\\\\x20*');

	# strip partitions off (remember changing $_ in map changes array)
	map s{
		( (?<base> / (sd|vd) [a-z]+) \d+ $) |
		( (?<base> / (sr) [0-9]+) p \d+ $)
	}{$+{base}}nx, @devs;

	return {
		map {
			my (undef, undef, undef, undef, undef, undef, $rdev,
				undef, undef, undef, undef, undef, undef
			) = stat($_) or die "stat($_): $!";

			$rdev, {node => $_}
		} @devs
	};
}

sub find_drives {
	local $_;

	# order of preference; will use the first one we find for a disk.
	# Note these are passed to glob, so they can't contain spaces
	my @name_order = qw(
		/dev/disk/by-id/wwn-*
		/dev/disk/by-id/*
		/dev/disk/by-path/*
	);

	# some things we can't blank and shouldn't try, like CDs and
	# ramdisks. Nor device-mapper stuff.
	my $blacklist = qr{^ /dev/(sr|ram|dm-) \d+ $}nx;

	my %disk;
	foreach my $cand (grep !m/-part\d+$/, map glob($_), @name_order) {
		my (undef, undef, undef, undef, undef, undef, $rdev,
			undef, undef, undef, undef, undef, undef
		) = stat($cand) or die "stat($cand): $!";
		next if exists $disk{$rdev};

		my $dev = abs_path($cand);
		next if $dev =~ $blacklist;

		# if removable and size=0, probably an empty e.g., SD card reader
		my $syspath = '/sys/dev/block/' . ($rdev>>8) . ':' . ($rdev&0xFF);
		my $removable = 0+read_text("$syspath/removable");
		my $size = 0+read_text("$syspath/size");
		next if $removable && !$size;

		$disk{$rdev} = { stable => $cand, node => $dev, sys => $syspath };
	}

	return \%disk;
}

1;


=head1 SYNOPSIS

    perl -MApp::ABAN -E 'exit App::ABAN->run()'

=head1 FUNCTIONS

=head2 PUBLIC API

=over 4

=item C<run>

Call this to run the app. Returns a Unix process exit code.

=back

=head2 INTERNAL API

=over 4

=item C<find_protected>

Tries to find drives that we should not ever wipe. Basically the goal is
not to wipe our own boot device. This is not a *general* way of finding
every device that might be in use on a Linux box, it's just designed to
be good enough for our live image, test VMs, etc.

=item C<find_drives>

Tries to find all the connected drives. This does not filter out the
protected from C<find_protected()>, but does filter stuff like CDs that
we can't wipe.

=item C<$_EXIT_CODE>

Default to 0, bit-or one of the exit constants against this if there is
a failure to report. This ultimately sets ABAN's exit status (returned
from C<run()>).

The constants are:

=over 4

=item C<_EX_BIT_DIED>

ABAN caught an exception (C<die> was called).

=item C<_EX_BIT_ABORTED>

The user canceled the wipe, e.g., with Control-C.

=back

=back

=head1 SEE ALSO

=over 4

=item L<ABAN>

Documentation for how to use ABAN is in the main L<ABAN> manual
page/perldoc.

=back
