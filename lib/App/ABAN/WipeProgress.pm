package App::ABAN::WipeProgress;
# ABSTRACT: Internal library to track wiper progress
use Moo;
use Carp;
use Time::HiRes qw(clock_gettime CLOCK_MONOTONIC);

=head1 SYNOPSIS

	my $work_to_do = 1024*1024;
	my $progress = App::ABAN::WipeProgress->new(
		remaining      => $work_to_do, # in bytes
		first_interval => 2,           # 2 seconds
		interval       => 60,          # 1 minute
	);
	while ($work_to_do) {
		$work_to_do -= do_some_work();
		$progress->remaining($work_to_do);
	}
	$progress->done

=head1 METHODS

=head2 Public Methods

=over 4

=item B<new>

Creates a WipeProgress object. Takes initial values for C<remaining>,
C<first_interval>, C<interval>, C<extra>, and C<_display> as arguments.

Note: C<_display> is for internal use by the test suite.

=item B<first_interval>

Accessor that gets the interval to the first progress display. Note that
this is read-only because it can't be changed after construction (there
is only one first display).

=cut 

has first_interval => (    #seconds
	is      => 'ro',
	default => sub { 2 },
);


=item B<interval>

Accessor that gets/sets how often progress is displayed. Note that
changes will only take effect after the current one is displayed (e.g.,
if it's 30 seconds until the next display and you change the interval to
5 minutes, then one will be displayed in 30s, but after that the next
one won't be until another 5 minutes).

=cut

has interval => (          # seconds
	is      => 'rw',
	default => sub { 60 },
);


=item B<remaining>

Accessor mainly to set how much work is remaining. This is expected to
be strictly decreasing, and setting this may cause progress info to be
displayed.

=cut

has remaining => (
	is => 'rw',
	trigger => 1,
	lazy => 1,
	default => sub { shift->_initial_work },
	init_arg => undef,
);


=item B<extra>

Accessor to get/set the extra info callback. If not undef, this coderef
is called (with the WipeProgress as its only argument) to get an extra
string to tack on to the progress output. Only called when progress is
actually displayed.

=cut

has extra => (
	is => 'rw',
);


=item B<overall_rate>

Accessor to get the overall rate (bytes/second) since the WipeProgress
was created.

=cut

has overall_rate => (    # bytes/sec
	is => 'rwp',
);


=item B<current_rate>

Accessor to get the rate between the previous progress display and the
most current one.

=cut

has current_rate => (    # bytes/sec
	is => 'rwp',
);


=item B<etr>

Returns a linear approximation of the time remaining based on the
overall rate and remaining work. Result in seconds.

=cut

sub etr {
	my $self = shift;
	return $self->overall_rate
		? $self->remaining / $self->overall_rate
		: undef;
}

=item B<done>

Call when you've finished all the work you're going to. Forces a final
rate computation and display.

=cut

sub done {
	my ($self) = @_;
	my $now = clock_gettime(CLOCK_MONOTONIC);

	$self->_update_progress($now, 1);
	$self->_display_progress($now);

	return;
}

=back

=head2 Internal Methods

=over 4

=item B<_initial_work>

Stores the amount of work specified at construction.

=cut

has _initial_work => (    # bytes
	is       => 'ro',
	init_arg => 'remaining',
);


=item B<_initial_time>

Stores the time at construction.

=cut

has _initial_time => (
	is      => 'ro',
	default => sub { clock_gettime(CLOCK_MONOTONIC) },
);


=item B<_next_progress>

The timestamp we'll next display progress on or after.

=cut

has _next_progress => (
	is   => 'rw',
	lazy => 1,
	default =>
		sub { my $self = shift; $self->_initial_time + $self->first_interval },
);


=item B<_last_work>

The amount of work remaining when we last displayed progress (or the
initial amount of work before that.)

=cut

has _last_work => (
	is => 'rw',
	lazy => 1,
	default => sub { shift->_initial_work },
);


=item B<_last_time>

The last time we displayed progress.

=cut

has _last_time => (
	is => 'rw',
	lazy => 1,
	default => sub { shift->_initial_time },
);


=item B<_display>

If set, called instead of the normal progress display function. Used by
the tests.

=cut

has _display => (
	is => 'rw',
);


=item B<_trigger_remaining>

Called when remaining is set. Used to update progress variables and
potentially display progress.

=cut
sub _trigger_remaining {
	my ($self) = @_;

	my $now = clock_gettime(CLOCK_MONOTONIC);
	my $display = ($now >= $self->_next_progress);

	$self->_update_progress($now, $display);
	$self->_display_progress($now) if $display;
	$self->_next_progress($now + $self->interval) if $display;

	return;
}

=item B<_update_progress>(I<now>, I<displaying>)

Update the progress variables. I<now> should be the current timestamp;
I<displaying> should be a true value if we're going to display progress
now (so the "current" values should be updated as well).

=cut

sub _update_progress {
	my ($self, $now, $displaying) = @_;
	my $remaining = $self->remaining;

	# opposite orders since we're counting down in remaining work and up
	# in time.
	$self->_set_overall_rate(
		($self->_initial_work - $remaining) / ($now - $self->_initial_time));

	if ($displaying) {
		$self->_set_current_rate(
			($self->_last_work - $remaining) / ($now - $self->_last_time));
		$self->_last_work($remaining);
		$self->_last_time($now);
	}

	return;
}


=item B<_display_progress>(I<now>)

Displays the progress to the user.

=cut

sub _display_progress {
	my ($self, $now) = @_;

	$self->_display
		and return $self->_display->($self, $now);

	my $extra
		= defined $self->extra
		? (': ' . $self->extra->($self))
		: '';

	printf STDERR "%0.0fm elapsed, %0.0f/%0.0f GiB done, %0.1f MB/sec (%0.1f now), ETR %ih%2im%s\n",
		($now-$self->_initial_time)/60,
		($self->_initial_work - $self->remaining)/(1024**3),
		$self->_initial_work/(1024**3),
		$self->overall_rate/(1024**2),
		$self->current_rate/(1024**2),
		int($self->etr/3600),
		int(($self->etr%3600)/60),
		$extra;

	return;
}

=back

=cut

1;
