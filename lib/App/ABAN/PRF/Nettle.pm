package App::ABAN::PRF::Nettle;
#ABSTRACT: Pseudo-random functions built with Nettle.
use strict;
use warnings;

require DynaLoader;
our @ISA = qw(DynaLoader);
__PACKAGE__->bootstrap;

1;

=head1 DESCRIPTION

This module provides some pseudo-random functions (PRFs) that are built
with Nettle ciphers. The PRFs are used to generate seekable
pseudo-random data to fill disks with.

Note that the Nettle functions were written first, but it turns out
OpenSSL anywhere from "slightly faster" to "much faster", so they're
typically used. ABAN picks automatically based on benchmark.

=head1 FUNCTIONS

=head2 Common Arguments

Both of these take the same long list of arguments:

B<func>(I<block>, I<key>, I<nonce>, I<zeroblock>, I<offset>, I<size>, I<output>)

=over 4

=item I<block>

The byte offset of the block we're generating pseudo-random data for.
This is just the initial value of the counter; it's perfectly fine to
change between the 4K and 1MB block sizes. You'll get the same result
for byte 4096 whether its byte zero of a block starting at 4096 or byte
4096 of a bigger block starting at 0.

=item I<key>

Key for the cipher. ABAN always uses a random key. Must be 256 bits (32
bytes), exactly.

=item I<nonce>

Nonce for the cipher. ABAN always uses a random nonce. Must be 64 bits
(8 bytes).

=item I<zeroblock>

Workaround for not being able to tell crypto libraries to not bother
encrypting anything and just spit out the keystream. This must consists
of NUL bytes and be at least as long as I<size>.

=item I<offset>

=item I<size>

=item I<output>

These three go together. I<output> is a scalar, which must be
pre-allocated, where the random data will be written (I<output> itself
is modified). I<offset> specifies the first byte in I<output> to modify;
0 means the start. I<size> says how many bytes to modify and must be
positive. B<Negative sizes are not allowed>; counting from the end is
not allowed here.

=back

=head2 Functions

=over 4

=item B<chacha_prf>

Uses ChaCha20 as a PRF. The counter comes directly from the byte offset
(I<block>).

=back

At one point, tested Nettle's AES-256-CTR, but it was far slower than
OpenSSL's, so dropped it. 

=head1 FUTURE DIRECTIONS

Nettle functions are mainly being kept now just in case we ever need a
non-OpenSSL build. 
