#define PERL_NO_GET_CONTEXT

#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"
#include <nettle/aes.h>
#include <nettle/chacha.h>
#include <nettle/ctr.h>

int chacha_prf(uint64_t block,
               unsigned char *key,   size_t key_buflen,
               unsigned char *nonce, size_t nonce_buflen,
               unsigned char *zero,  size_t zero_buflen,
               size_t off, size_t size, unsigned char *out, size_t out_buflen) {

	struct chacha_ctx ctx;

	if (key_buflen != CHACHA_KEY_SIZE) {
		croak("Key is %zu bytes, not expected %zu",
		      key_buflen, (size_t)CHACHA_KEY_SIZE);
		return 0;
	}

	if (nonce_buflen != CHACHA_NONCE_SIZE) {
		croak("Nonce is %zu bytes, not expected %zu",
		      nonce_buflen, (size_t)CHACHA_NONCE_SIZE);
		return 0;
	}

	if (zero_buflen < size) {
		croak("Zero block is too small (is %zu, need %zu)",
		      zero_buflen, size);
		return 0;
	}

	if (off + size > out_buflen) {
		croak("Asked to overflow, offset=%zu, length=%zu, buffer_size=%zu",
		      off, size, out_buflen);
		return 0;
	}

	chacha_set_key(&ctx, key);
	chacha_set_nonce(&ctx, nonce);

	/* Nettle doesn't seem to provide an easy way to set the block counter, so we have to do this. */
	ctx.state[12] = block & 0xFFFFFFFFul;
	ctx.state[13] = (block>>32) & 0xFFFFFFFFul;

	chacha_crypt(&ctx, size, out+off, zero);

	return 1;
}


MODULE = App::ABAN::PRF::Nettle				PACKAGE = App::ABAN::PRF::Nettle

PROTOTYPES: DISABLED

int
chacha_prf(UV block,                                      \
           char *key, size_t length(key),                 \
           char *nonce, size_t length(nonce),             \
           char *zeroblock, size_t length(zeroblock),     \
           size_t off, size_t size, SV *out_sv)
PREINIT:
	char *out;
	size_t out_len;
CODE:
	if (sizeof(block) < 8)
		croak("UV isn't 64-bit or larger; FIXME");
	out = SvPV_force(out_sv, out_len);
	RETVAL = chacha_prf(block,
	                    (unsigned char *)key, XSauto_length_of_key,
	                    (unsigned char *)nonce, XSauto_length_of_nonce,
	                    (unsigned char *)zeroblock, XSauto_length_of_zeroblock,
	                    off, size, (unsigned char *)out, out_len);
OUTPUT:
	RETVAL
