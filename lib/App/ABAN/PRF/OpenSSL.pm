package App::ABAN::PRF::OpenSSL;
#ABSTRACT: Pseudo-random functions built with OpenSSL.
use strict;
use warnings;

require DynaLoader;
our @ISA = qw(DynaLoader);
__PACKAGE__->bootstrap;

1;

=head1 DESCRIPTION

This module provides some pseudo-random functions (PRFs) that are built
with OpenSSL ciphers. The PRFs are used to generate seekable
pseudo-random data to fill disks with.

=head1 FUNCTIONS

Both of these take the same long list of arguments:

B<func>(I<block>, I<key>, I<nonce>, I<zeroblock>, I<offset>, I<size>, I<output>)

This is the same list of arguments as the Nettle versions; see
L<App::ABAN::PRF::Nettle> for details.

=over 4

=item B<chacha_prf>

This uses the ChaCha20 cipher directly as a PRF. The byte offset
is used as the counter.

=item B<aes256_prf>

This uses AES with a 256-bit key in counter mode (AES-256-CTR) as a PRF.
The byte offset is used as the counter.

=back
