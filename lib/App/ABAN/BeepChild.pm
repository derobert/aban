package App::ABAN::BeepChild;
#ABSTRACT: ABAN internal POE session to beep a disk count
use 5.028;
use warnings qw(all);
use strict;

use POE qw(Wheel::Run);

sub create {
	my (undef, $ident, $disk_count) = @_;

	my @cmd;
	my %handlers = (
		_start       => 'start_session',
		child_signal => 'child_signal',
	);

	return POE::Session->create(
		args => [ $ident, $disk_count ],
		package_states => [ __PACKAGE__, \%handlers ]
	);
}

sub start_session {
	$_[HEAP]{ident} = $_[ARG0];
	$_[KERNEL]->alias_set("BeepChild-$_[HEAP]{ident}");

	# beep refuses to run since ABAN obtained root with sudo. So hide
	# that fact from it.
	my @cmd = (
		qw(env -u SUDO_COMMAND -u SUDO_USER -u SUDO_UID -u SUDO_GID
			beep -f 1568 -l 250 -D 400 -r), $_[ARG1]);
	my $child = POE::Wheel::Run->new(
		Program => \@cmd,
		NoStdin => 1,
	);
	$_[KERNEL]->sig_child($child->PID, 'child_signal');
	$_[HEAP]{child} = $child;

	return;
}

sub child_signal { 
	# remove both refs to allow GC
	$_[HEAP]{child} = undef; 
	$_[KERNEL]->alias_remove("WipeChild-$_[HEAP]{ident}");
}

1;

=head1 DESCRIPTION

This module is a L<POE::Session> to run the "beep" command to indicate
how many disks we're wiping. You determine the number of disks by
counting the beeps. This is to allow headless operation.

=head1 SEE ALSO

=over 4

=item L<ABAN>

For detail about what ABAN is, see the main L<ABAN> manual page/perldoc.

=back
