package App::ABAN::Util;
#ABSTRACT: ABAN internal C utilities.
use strict;
use warnings;

require DynaLoader;
require Exporter;
our @ISA = qw(DynaLoader Exporter);
our @EXPORT_OK = qw(get_blockdev_size);

__PACKAGE__->bootstrap;

1;

=head1 SYNOPSIS

    use App::ABAN::Util qw(get_blockdev_size);
    sysopen my $fh, '/dev/sda', O_RDONLY;      # prob. needs root
    my $size = get_blockdev_size(fileno($fh));

=head1 FUNCTIONS

All these functions are exportable.

=over 4

=item B<get_blockdev_size>(I<fileno>)

Call BLKGETSIZE64 on a block device, which should already be open. Pass
the file number (not file handle); i.e., pass C<fileno($fh)> as I<fileno>.

=back
