#define PERL_NO_GET_CONTEXT

#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"
#include <linux/fs.h>
#include <sys/ioctl.h>
#include <string.h>

UV get_blockdev_size(int fd) {
	UV size;
	int res;

	if (sizeof(size) < 8) {
		croak("BUG: UV isn't at least 64-bit");
	}

	res = ioctl(fd, BLKGETSIZE64, &size);
	if (res != 0)
		croak("ioctl BLKGETSIZE64: %s", strerror(errno));

	return size;
}

MODULE = App::ABAN::Util    PACKAGE = App::ABAN::Util
PROTOTYPES: DISABLED

UV
get_blockdev_size(int fd)
CODE:
	if (sizeof(RETVAL) < 8)
		croak("UV isn't 64-bit or larger; FIXME");
	RETVAL = get_blockdev_size(fd);
OUTPUT:
	RETVAL
