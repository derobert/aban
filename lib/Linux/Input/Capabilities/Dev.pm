package Linux::Input::Capabilities::Dev;
#ABSTRACT: Capabilities of a single input device
use 5.028;
use strict;
use warnings;
use Moo;
use Bit::Vector;
use Carp;
use File::chdir;
use File::Slurper qw(read_text);
use Linux::Input::Capabilities::Constants qw(%_REVERSE_TABLE);
use namespace::autoclean;
use warnings::register;
use autodie qw(opendir readdir closedir);

has name => (
	is => 'rwp',
	init_arg => undef,
);

has _bits => (
	is => 'rw',
	trigger => sub { shift->_clear_decoded },
);

has _decoded => (
	is => 'ro',
	lazy => 1,
	clearer => '_clear_decoded',
	builder => sub { +{ } },
);

has _dir => (
	is => 'ro',
	init_arg => 'dir',
	required => 1,
);

sub BUILD {
	my ($self, $args) = @_;
	local $CWD = $CWD;

	push @CWD, $self->_dir;
	chomp(my $name = read_text('name'));
	$self->_set_name($name);

	push @CWD, 'capabilities';
	opendir my $dh, '.';
	my @entries = readdir $dh;
	closedir $dh;

	my %bits;
	foreach my $ent (@entries) {
		next if $ent =~ /^\./;
		if ($ent =~ /^(abs|ev|ff|key|led|msc|rel|snd|sw)$/n) {
			$bits{$ent} = _parse_bits(read_text($ent));
		} else {
			warnif("Unknown sysfs capability type '$ent' ignored; module may need updating to support it");
		}
	}

	$self->_bits(\%bits);
}

sub get_supported {
	my ($self, $type) = @_;
	local $_;
	
	my $v = $self->_bits->{$type}
		or croak "Unknown type '$type'";

	unless (exists $self->_decoded->{$type}) {
		$self->_decoded->{$type} = [
			map $_REVERSE_TABLE{uc $type}{$_} // "#UNKNOWN#$_",
			grep $v->contains($_),
			0 .. ($v->Size - 1)];
	}

	return $self->_decoded->{$type};
}

sub is_supported {
	my ($self, $type, $report) = @_;

	my $v = $self->_bits->{$type}
		or croak "Unknown type '$type'";

	return $v->Size > $report && $v->contains($report);
}

sub count_supported {
	my ($self, $type, @reports) = @_;
	local $_;

	my $v = $self->_bits->{$type}
		or croak "Unknown type '$type'";

	my $sz = $v->Size;
	return scalar grep $sz > $_ && $v->contains($_), @reports;
}

sub _parse_bits {
	my ($text) = @_;
	local $_;

	# Kernel formats this as groups separated by spaces. Each group is
	# 32 or 64 bits long, depending on architecture. At least on Debian
	# i386 and amd64, it matches Config longsize, and even better
	# Bit::Vector->Word_Bits.
	#
	# However, the kernel doesn't print leading 0s, so we have to pad it
	# to get it to a nice hex string.
	my $group_nybbles = Bit::Vector->Word_Bits() / 4;
	my @groups = map '0'x($group_nybbles-length($_)).$_, split(/\s+/, $text);
	my $size = @groups * Bit::Vector->Word_Bits();
	my $hex = join('', @groups);

	# sanity check because if we messed up, we're about to produce
	# nonsense. This should never happen.
	if ($size / 4 != length($hex)) {
		die "BUG: Hex isn't expected length; expected ${\($size/4)} got ${\length($hex)} for $hex";
	}

	return Bit::Vector->new_Hex($size, $hex);
}

1;

__END__

=head1 SYNOPSIS

See L<Linux::Input::Capabilities>.

=head1 DESCRIPTION

Class returned by the various device methods of
L<Linux::Input::Capabilities> to represent a single input device. Not intended to be used directly.

=head1 PUBLIC METHODS

=over 4

=item B<name>

Returns the supposedly human-readable name from sysfs.

=item B<get_supported>(I<type>)

Returns all the supported events/reports of a given type. I<type> should be one of:

=over 4

=item C<abs>

Absolute position (e.g., graphic tablet, touch screen)

=item C<ev>

libinput stuff, also tells you which other types the device should have.
E.g., if it has C<EV_KEY>, then C<key> should have something.

=item C<ff>

"force feedback", whatever that is. Don't have any of these to test with
on my workstation.

=item C<key>

Keys (like on a keyboard) plus some buttons called keys (like
C<KEY_CAMERA> on a USB webcam, or C<KEY_POWER> on the front of the
machine), plus some buttons called buttons (like C<BTN_MOUSE>, the main
mouse button).

=item C<led>

Blinky lights.

=item C<msc>

Stuff that didn't fit elsewhere.

=item C<rel>

Relative movement (e.g., a mouse or trackball).

=item C<snd>

Predefined sounds a device can make, like C<SND_BELL> on a PC speaker.
You'll have to ask Linux devs why a PC speaker is an input device.

=item C<sw>

Switches, like C<SW_LINEIN_INSERT> which tells you something is plugged
in to the line-in port (sound card).

=back

The strings returned should match the constant name I<except when its an
unknown bit>. Unknown bits are reported a C<#UNKNOWN#1234> where I<1234>
is the bit number, counting from 0.

=item B<is_supported>(I<type>, I<report>)

Check if a given I<report> is supported. See L<get_suported> for
I<type>. Returns a true value if supported, false otherwise.

=item B<count_supported>(I<type>, I<report>, ...)

Given a list of I<report>s of the same I<type>, count how many of them are supported and return that. If none are, return 0. If you want to check if all are supported, make sure the count matches:

    my @wanted = (KEY_Y, KEY_N);
    if (@wanted == $dev->count_supported(key => @wanted)) {
        say "all supported";
    }

See L<get_supported> for the valid I<type>s.

=back
