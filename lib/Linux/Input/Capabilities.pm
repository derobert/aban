package Linux::Input::Capabilities;
#ABSTRACT: Check capabilities of Linux input devices using sysfs.
use 5.028;
use strict;
use warnings;
use autodie qw(opendir readdir closedir);
use Carp;
use Exporter qw(import);
use File::chdir;
use Linux::Input::Capabilities::Dev;
use Moo;
use namespace::autoclean;

has sysfs_root => (
	is => 'rw',
	default => '/sys',
	trigger => sub { shift->reload() },
);

has _devices => (
	is => 'ro',
	lazy => 1,
	clearer => '_clear_devices',
	builder => '_build_devices',
);

sub BUILD {
	my ($self, $args) = @_;

	# force reading sysfs, so we get errors on new not first use.
	$self->_devices;

	return;
}

sub _build_devices {
	my ($self) = @_;

	local $CWD = $self->sysfs_root;
	push @CWD, 'class', 'input';

	opendir my $dh, '.';
	my @entries = readdir($dh);
	closedir($dh);

	my %devs;
	foreach my $entry (@entries) {
		next unless $entry =~ /^input(\d+)$/;
		$devs{$1} = Linux::Input::Capabilities::Dev->new(dir => $entry);
	}

	return \%devs;
}

sub reload {
	my ($self) = @_;

	$self->_clear_devices;
	$self->_devices;

	return;
}

sub list {
	my ($self) = @_;

	return keys %{$self->_devices};
}

sub get {
	my ($self, $input) = @_;

	my $d = $self->_devices->{$input}
		or croak "Unknown input device $input";

	return $d;
}

sub find {
	my ($self, $type, $min, @reports) = @_;

	my @res;
	while (my ($num, $dev) = each %{$self->_devices}) {
		push @res, $dev if $min <= $dev->count_supported($type, @reports);
	}

	return @res;
}

1;

__END__

=head1 SYNOPSIS

 use Linux::Input::Capabilities;

 my $devices = Linux::Input::Capabilities->new();
 my $dev = $devices->get(0);

=head1 METHODS

=over 4

=item B<new>()

Create a new object. This involves parsing the available input devices
from I<sysfs_root>, which defaults to F</sys>.

Arguments are passed in the normal hash/hashref manner, and are:

=over 4

=item I<sysfs_root>

Where sysfs is mounted, which is normally F</sys> (the default). Mainly
this is for the tests to pass in test data.

=back

=item B<sysfs_root>([I<newval>])

Get (if no value is passed) or set (if I<newval> is passed) the sysfs
mount point. Settings this forces a L<reload>.

=item B<reload>()

Re-read the available devices and their capabilities from sysfs. Note
that if you have any single-device objects (e.g., from L<get>), those
are not updated; you need to re-fetch them. So:

    $dev_old = $devices->get(0);
    $devices->reload;
    $dev_new = $devices->get(0);

after that, C<$dev_old> and C<$dev_new> may be I<completely different>.

=item B<list>()

Return the list of devices (the numbers you'd pass to L<get>).

=item B<get>(I<device>)

Get the L<Linux::Input::Capabilities::Dev> for I<device>.

=item B<find>(I<type>, I<minimum>, I<report>,...)

Find all the devices which support at least I<minumum> reports out of
the passed I<report>, all of them being of type I<type>.

=back

=head1 CONSTANTS

Numerous constants from F<linux/input.h> are exported by
L<Linux::Input::Capabilities::Constants>: all the ones corresponding the
events input devices can produce. They're named exactly as the C define
is; e.g., C<KEY_Y>. There are over 700 of these, covering keys, buttons,
mouse movement, plug/unplug, etc., so they're not listed. (The easiest
way to get a list is to just look at the module source code or its
C<@EXPORT_OK>. Note that also contains underscore-prefixed things which
are only for internal use.)

=head1 SEE ALSO

=over 4

=item F<Documentation/input/event-codes.txt>

The kernel documentation (available online at
L<https://www.kernel.org/doc/Documentation/input/event-codes.txt>)
contains some stuff at least hints at how this is supposed to work.

=back
