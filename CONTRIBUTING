Contributions Welcome
=====================

Pull requests, patches, bug reports, feature ideas, artwork, web design,
etc. all are welcome. Please feel free to open an issue or send me an
email to Anthony DeRobertis <anthony@derobert.net> to discuss. (XMPP to
the same should work as well, but email is preferred). I'm also derobert
on Freenode and OFTC.


Source
======

Code Organization
-----------------

The source code contains a few largely independent parts. The first (and
more interesting one) is ABAN, a Perl application. It lives in bin/ and
lib/ with tests in t/. It's built with Dist::Zilla, and you don't need
to know much about the Live CD/USB stuff to work on it.

It also has a local Dist::Zilla plugin in author/lib and data for the
tests in t-data/.

The second is configuration and some files for building the Debian Live
CD/USB image. These can be found in live-build/. Working on this doesn't
require much if any knowledge of Perl.

The third is stuff for the web page, found in pages/. Other than the
utility script in pages/bin/, this is all HTML/CSS.

This is all glued together by the GitLab CI configuration
(.gitlab-ci.yml). There is a Dockerfile in the build-images branch which
builds some Debian images with a the packages for the build-* steps
pre-installed; this is just to speed up CI (by about 2x currently).


git Branch Usage
----------------

"master" is (hopefully) always producing a useful image, and is
protected against force-pushes so should always fast-forward. GitLab CI
produces images from all commits, but except for tags they're set to
expire after a day. Releases are done with v* starts, which also builds
and updates the home page (via GitLab Pages).

Non-trivial bug fixes and features are done on branches. Branches,
especially ones with "wip" in their name, may have history rewritten. If
you want to base something on one of them (instead of master), please
let me know so that I can avoid rewrites on that branch.
